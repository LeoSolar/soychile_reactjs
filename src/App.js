import React, {Component} from 'react'
import './css/main.css'
import Diario from './Calls/hxhDiario'
import Header from './Controls/Comun/header'
import Noticias from './Controls/Portada/noticiasCentral'
import hxhImg from './img/diarios/22.png'

function App() {
  return (
      <div className="container-fluid">
        <div className="row">
        <div className="box-content box-diario">
          <div className="box-content-header box-content-header-diario clearfix">
            <div className="header-container header-container-diario is-fixed">
              <div>
              <a href="http://www.hoyxhoy.cl/" className="header-logo header-logo-diario"><img src={hxhImg} alt="test" /></a>
              </div>
            </div>
         </div>
         <div>
           <div className="paginas-diario">
            <div className="can-be-fixed-column js-fixeable"><div>
              <Diario />
            </div>
           </div>
           </div>
         </div>
        </div>
        <div className="box-content box-ciudad">
          <Header />
          <div className="flex-layout" style={{position:'relative'}}>
            <div className="box-content-body">
              <div className="destacadas-content">
                <Noticias></Noticias>
              </div>
            </div>
            <div className="banner-fixed-container"></div>
          </div>
        </div>
      </div>
      </div>
  );
}

export default App;
