import React, { Component } from 'react';

class Elastic extends Component {
    
    state = {
        source : {}
    }

    componentDidMount(){
        //fetch("http://cache-elastic-pandora.ecn.cl/mediosregionales/noticia/"+this.props.idnoticia)
        fetch("http://cache-elastic-pandora.ecn.cl/mediosregionales/noticia/_search?q=id:"+this.props.idnoticia+"&publicada:true")
        .then(data => data.json())
        .then(data => this.setState({source: data.hits.hits[0]._source}))
        .catch(console.log);
    }

    vFecha(fechaModificacion){
        var date = { currentTime: new Date(), fechapandora: new Date(fechaModificacion)  };
        var hoy = date.currentTime.getFullYear()+'-'+(date.currentTime.getMonth()+1)+'-'+date.currentTime.getDate();
        var pandora = date.fechapandora.getFullYear()+'-'+(date.fechapandora.getMonth()+1)+'-'+date.fechapandora.getDate();
        if(hoy != pandora){
            var DiffTime = date.currentTime.getTime() - date.fechapandora.getTime();
            var DiffDays = DiffTime / (1000 * 3600 * 24);
            if(DiffDays == 1){
                return "Ayer";
            }else{
                return "Archivo";
            }
        }else{
            var h = date.fechapandora.getHours();
            var m = date.fechapandora.getMinutes();
            m = this.checkTime(m);
            return h + ":" + m;
        }
    }

    checkTime(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    remoAcc(s){
        var r=s || '';
        r = r.replace(new RegExp("\\s", 'g'),"");
        r = r.replace(new RegExp("[àáâãäå]", 'g'),"a");
        r = r.replace(new RegExp("æ", 'g'),"ae");
        r = r.replace(new RegExp("ç", 'g'),"c");
        r = r.replace(new RegExp("[èéêë]", 'g'),"e");
        r = r.replace(new RegExp("[ìíîï]", 'g'),"i");
        r = r.replace(new RegExp("ñ", 'g'),"n");                            
        r = r.replace(new RegExp("[òóôõö]", 'g'),"o");
        r = r.replace(new RegExp("œ", 'g'),"oe");
        r = r.replace(new RegExp("[ùúûü]", 'g'),"u");
        r = r.replace(new RegExp("[ýÿ]", 'g'),"y");
        r = r.replace(new RegExp("\\W", 'g'),"");
        r = r.toLowerCase();
        return r;
    }


    render() {
        return (
                <div className="media-text">
                    <h4 className="media-heading">
                        <a href={this.state.source.permalink}>{this.state.source.titulo}</a>
                    </h4>
                    <div className="media-media">
                        <a href={this.state.source.permalink}>
                            <img className=" lazyloaded" src="img.jpg" width="270" data-src="" alt="" />
                        </a>
                    </div>
                    <div className="media-body">
                        <div className="media-body-time">{this.vFecha(this.state.source.fechaModificacion)} </div>
                        {this.state.source.bajada && this.state.source.bajada.map((items,i) => {
                           const texto = items.texto;
                           return <span className="media-desc-text" key={i}>{texto}</span>;
                       })}
                       <span className="media-meta-via">
                            <i className={'icon-city icon-city-'+this.remoAcc(this.state.source.seccion)}></i>
                            <a href={'//www.soychile.cl/'+this.remoAcc(this.state.source.seccion)+'/'}>soy<strong>{this.state.source.seccion}</strong></a>
                        </span>
                    </div>
                </div>
        );
    }
}

export default Elastic;
