import React, { Component } from 'react';

class Diario extends Component {
    state = {
        apidata : []
    }

    componentDidMount(){
        fetch("http://www.hoyxhoy.cl/endpoints/for-soy.php?action=get-latest&size=380")
        .then(data => data.json())
        .then(data => this.setState({apidata: data}))
        .catch(console.log);
    }

    render() {
        return (
            this.state.apidata.map((data,i) => (
                <div className="box box-130" key={i}>
                    <span className="page-number">{data.page}</span>
                    <a className="page-link" href={data.link}>
                        <img className="page-image lazyloaded" src={data.img} data-src={data.img} alt="test" />
                    </a>
                </div>
            ))
        );
    }
}

export default Diario;
