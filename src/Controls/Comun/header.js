import React, { Component } from 'react';
import logo from '../../img/ciudades/10.png';

class header extends Component {
    render (){
        return (
            <div className="header-container header-container-ciudad is-fixed">
                <div>
                    <div className="nav-toggle-padding">
                        <button type="button" className="btn nav-toggle pull-left" data-action="open-sidebar">
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                    </div>
                    <a href="/" className="header-logo header-logo-ciudad">
                        <img src={logo} />
                    </a>
                    <ul className="top-nav top-nav-diario list-unstyled">
                        <li className="jc-btn-tarifas">demas cosas</li>
                        <li className="jc-btn-tarifas" style={{display:'none'}}>demas cosas</li>
                    </ul>
                </div>
                <div className="sidebar-header-title"></div>
            </div>
        );
    }

}

export default header;