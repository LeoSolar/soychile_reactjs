import React, { Component } from 'react';
import notiPortada from '../../ExtrasNOSUBIR/noticias_portada.json'
import ElasticQ from '../../Calls/queryElastic'

const dataJson = notiPortada;

class noticiascentral extends Component {
    render() {
        return (
            <div className="home-section home-section--1">
            <section>
                {dataJson.map((s,i) => (
                    <div className="media media-destacadas" date-type="text" data-noticia={s.id} key={i}>
                        <div className="clearfix">
                            <ElasticQ idnoticia={s.id} />
                        </div>
                        <div className="media-footer"></div>
                    </div>
                ))}
            </section>
            <aside></aside>
            </div>
        );
    }
}

export default noticiascentral;
